#!/bin/bash

MYSHELL="zsh"
MYNVIM="v0.10.3"
MYGNOME="kali"

TSVERSION="v0.25.1"
NVMVERSION="v0.40.1"

FONTS=("JetBrainsMono" "Meslo" "FiraCode" "DejaVuSansMono")
FONTVERSION="v3.2.1"

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

cd "$SCRIPTPATH" || exit

function requirements()
{
    echo -en "- Checking dependencies"

    if [ ! -d ~/.local/bin ]; then
        echo "- Creating ~/.local/bin"
        mkdir ~/.local/bin
        export PATH=$PATH:$HOME/.local/bin
    fi

    binaries=("git" "curl" "wget" "fc-cache" "dconf" "pip3" "unzip")
    install=()
    for x in "${binaries[@]}"; do
        if [[ ! -x $(which "$x") ]]; then
            case "$x" in
                "fc-cache") install+=("fontconfig");;
                "dconf")    install+=("dconf-cli");;
                "pip3")     install+=("python3-pip");;
                * )         install+=("$x");;
            esac
        fi
    done
    if ((${#install[@]})); then
        echo -e "\n! missing binaries, please run: apt --no-install-recommends install ${install[*]}"
        exit
    else
        echo " ok"
    fi
}

function checkbin()
{
    if [[ -z $1 ]]; then
        echo "! binary not define..."
        return 1
    fi
    if [[ ! -x $(which "$1") ]]; then
        echo "! $1 not found, please run: apt install $1"
        return 1
    fi
    return 0
}

function install_fonts ()
{
    URL="https://github.com/ryanoasis/nerd-fonts/releases/download"
    FONTPATH="$HOME/.local/share/fonts"

    [[ ! -d $FONTPATH ]] && mkdir -p "$FONTPATH"

    touch "$FONTPATH/.version"
    if ! grep -q $FONTVERSION "$FONTPATH/.version"; then
        echo "- Install fonts $FONTVERSION"

        for FONT in "${FONTS[@]}"; do
            rm -rf "${FONTPATH:?}/$FONT"
            mkdir -p "$FONTPATH/$FONT"

            cd "$FONTPATH/$FONT" || return

            wget -q "$URL/$FONTVERSION/$FONT.zip"
            unzip -q "$FONT.zip" && rm "$FONT.zip"

            [[ -x $(which fc-cache) ]] && fc-cache -f
        done
        echo $FONTVERSION > "$FONTPATH/.version"
    else
        echo "- Install fonts $FONTVERSION"
    fi
    cd "$SCRIPTPATH" || return
}

function install_tmux ()
{
    SRCPATH="./config"
    echo "- Install tmux"
    checkbin "tmux" || return
    cp -rf "$SRCPATH/tmux.conf" ~/.tmux.conf
}

function install_screen ()
{
    SRCPATH="./config"
    echo "- Install screen"
    checkbin "screen" || return
    cp -rf "$SRCPATH/screenrc" ~/.screenrc
}

function install_shell ()
{
    SRCPATH="./shell"
    GPROMPT="https://github.com/magicmonty/bash-git-prompt.git"
    PL10K="https://github.com/romkatv/powerlevel10k.git"
    SHELLPATH="$HOME/.shell"

    echo "- Install shell $MYSHELL"
    checkbin "$MYSHELL" || return

    [[ ! -d "$SHELLPATH" ]] &&  mkdir "$SHELLPATH"

    case "$MYSHELL" in
        "bash") [ ! -d "$SHELLPATH/b10k" ] && git clone $GPROMPT "$SHELLPATH/b10k" -q;;
        "zsh")  [ ! -d "$SHELLPATH/p10k" ] && git clone --depth=1 $PL10K "$SHELLPATH/p10k" -q;;
    esac

    cp -rf "$SRCPATH/common" "$SHELLPATH/"
    cp -rf "$SRCPATH/$MYSHELL" "$SHELLPATH/"

    if [[ $MYSHELL != $(echo "$SHELL" | awk -F/ '{print $NF}') ]]; then
        chsh -s "$(which "$MYSHELL")"
        echo "logout to use your new shell: $MYSHELL"
    fi

    [[ ! -d "$SHELLPATH/bakup" ]] && mkdir -p "$SHELLPATH/bakup"
    case "$MYSHELL" in
        "bash")
            if [[ ! -f "$SHELLPATH/bakup/.bashrc" ]]; then
                [[ -f $HOME/.bashrc ]] && cp "$HOME/.bashrc"  "$SHELLPATH/bakup/"
            fi
            cp -rf "$SRCPATH/bashrc" ~/.bashrc
            echo "source ~/.bashrc" > ~/.bash_profile
            ;;
        "zsh")
            if [[ ! -f "$SHELLPATH/bakup/.zshrc" ]]; then
                [[ -f $HOME/.zshrc ]] && cp "$HOME/.zshrc"  "$SHELLPATH/bakup/"
            fi
            cp -rf "$SRCPATH/zshrc" ~/.zshrc
            [[ $MYSHELL != $(echo "$SHELL" | awk -F/ '{print $NF}') ]] &&  exec zsh
            ;;
    esac
}

function install_git ()
{
    SRCPATH="./config"
    GITPATH="$HOME/.shell/git"
    echo "- Install gitconfig"

    cp -rf "$SRCPATH/gitconfig" ~/.gitconfig

    if [[ ! -d "$GITPATH/" ]]; then
        mkdir -p "$GITPATH/"

        USERNAME=$(git config --get user.name 2> /dev/null)
        USERMAIL=$(git config --get user.email 2> /dev/null)

        [[ -z $USERNAME ]] && read -p 'git user.name : ' USERNAME
        [[ -z $USERMAIL ]] && read -p 'git user.email : ' USERMAIL

        printf '%s\n' \
            "# Default your user config" \
            "[user]" \
            "name = $USERNAME" \
            "email = $USERMAIL" \
            "" \
            "# Override user config per remote" \
            "#[includeIf \"hasconfig:remote.*.url:*://git@gitlab.com*/**\"]" \
            "#path = ~/.shell/git/gitlab" \
            "" \
            "# Add any custom config" \
            "" \
            "# vim: syntax=gitconfig" > "$GITPATH/config"
    fi
}

function install_golang ()
{
    URL="https://dl.google.com/go"
    GODOC="https://godoc.org/golang.org/dl"
    LATEST=$(curl -sL $GODOC | grep -oP "go\d+\.\d+(\.\d+)?\s" | awk '{$1=$1};1' | sort -V | uniq | tail -n 1)
    CURRENT=""

    [[ -z $GOROOT ]] && export GOROOT=$HOME/.local/go
    [[ -z $GOPATH ]] && export GOPATH=$HOME/sources/golang
    [[ -z $GOBIN ]] && export GOBIN=$HOME/sources/golang/bin

    [[ ! -d $GOROOT ]] && mkdir -p "$GOROOT"
    [[ ! -d $GOPATH ]] && mkdir -p "$GOPATH"/{bin,mod,pkg,src}

    [[ -f $GOROOT/bin/go ]] && CURRENT=$("${GOROOT}/bin/go" version | grep -oP "go\d+\.\d+(\.\d+)?")

    echo "- Install golang $LATEST"

    if [[ ${CURRENT} == "${LATEST}" ]]; then
        return
    else
        rm -rf "$GOROOT"
        wget -q -c "${URL}/${LATEST}.linux-amd64.tar.gz" -O - | tar -xz -C "$HOME/.local/"
        "$GOROOT/bin/go" install github.com/nsf/gocode@latest
    fi
}

function install_cheat ()
{
    SRCPATH="./cheat"
    CHEATPATH="$HOME/.config/cheat"
    CHEATSHEETS="https://github.com/cheat/cheatsheets"

    echo "- Install cheat"

    if [[ -z $GOROOT ]]; then
        echo "! golang not found, please run: ./install --golang"
        exit 0
    fi

    if [[ ! -d "$CHEATPATH/" ]]; then
        mkdir -p "$CHEATPATH/cheatsheets"
        git clone --quiet $CHEATSHEETS "$CHEATPATH/cheatsheets/community" > /dev/null
        #git clone git@gitlab.com:elnox/cheat.git "$CHEATPATH/cheatsheets/yoda" > /dev/null
        cp "$SRCPATH/cheat.yml" "$CHEATPATH/conf.yml"
    fi

    "${GOROOT}/bin/go" install github.com/cheat/cheat/cmd/cheat@latest
}

function install_neovim ()
{
    SRCPATH="./neovim"
    SITURL="https://github.com/tree-sitter/tree-sitter/releases/download/$TSVERSION/tree-sitter-linux-x64.gz"
    NVMURL="https://raw.githubusercontent.com/nvm-sh/nvm/$NVMVERSION/install.sh"
    URL="https://gitlab.com/elnox/neovim/-/raw/master/utils/install"

    if [[ ! -x $(which "tree-sitter") ]]; then
        wget -q "$SITURL"
        gzip -d tree-sitter-linux-x64.gz
        chmod +x tree-sitter-linux-x64 && mv tree-sitter-linux-x64 ~/.local/bin/tree-sitter
    fi

    if [[ ! -x $(which "npm") ]]; then
        bash <(curl -sL $NVMURL) > /dev/null
        export NVM_DIR="$HOME/.nvm"
        [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
        nvm install node > /dev/null
    fi

    if [[ ! -x $(which "nvim" | grep "${HOME}") ]]; then
        bash <(curl -s $URL) --release "$MYNVIM" --config lazy
    else
        echo "- Install neovim $MYNVIM lazy"
    fi
}

function install_gnome ()
{
    SRCPATH="./gnome"
    if [[ $DESKTOP_SESSION =~ ^gnome ]] || [[ $DESKTOP_SESSION =~ ^ubuntu ]]; then
        if [[ -z "$MYGNOME" ]]; then
            echo "! gnome conf not set, choose one of: init, kali..."
            return 1
        fi
        [[ $DESKTOP_SESSION =~ ^ubuntu ]] && gnome="ubuntu"
        if [[ ! -f $SRCPATH/$MYGNOME.cfg ]]; then
            echo "! gnome conf $MYGNOME not found, choose one of: init, kali..."
            return 1
        fi
        echo "- Install gnome $MYGNOME"
        dconf load / < "$SRCPATH/$MYGNOME.cfg"
    fi
}

function usage ()
{
    echo "$0 --shell <bash || zsh> --gnome <init || kali || ubuntu> --neovim <stable || nightly || v0.7.2>"
    echo
    exit
}

function main ()
{
    echo "*** Install dotfiles for user : $USER"

    # check requirements
    requirements

    # check args
    while [[ $# -ne 0 ]]; do
        arg="$1"; shift
        case "$arg" in
            --shell)    MYSHELL="$1"; shift;;
            --neovim)   MYNVIM="$1"; shift;;
            --gnome)    MYGNOME="$1"; shift;;
            --help)     usage;;
            * ) [[ $arg =~ \-+.* ]] && usage "$arg unknown"
        esac
    done

    # auto install
    install_fonts
    install_tmux
    install_screen
    install_git
    install_golang
    install_cheat
    install_shell
    install_neovim
    install_gnome

    if [[ -d ~/.local/bin ]] && [[ ! -L ~/.local/bin/dotfiles ]]; then
        echo "*** Create symlink : ~/.local/bin/dotfiles"
        ln -sf "$(pwd)/install"  ~/.local/bin/dotfiles ;
    fi
}

main "$@"
