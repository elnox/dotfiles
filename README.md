# dotfiles

## Requirements

```bash
# common
apt install curl dconf-cli fontconfig git unzip screen tmux

# gnome
apt install gnome-tweaks gnome-shell-extension-manager chrome-gnome-shell

# zsh
apt install zsh zsh-syntax-highlighting zsh-autosuggestions

# neovim
apt --no-install-recommends install universal-ctags python3-pip python3-venv ripgrep fd-find xclip build-essential python3-pynvim

# lua / ruby / java / rust / julia
apt install lua5.4 luarocks ruby-full openjdk-19-jdk
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
wget -qO- https://julialang-s3.julialang.org/bin/linux/x64/1.8/julia-1.8.1-linux-x86_64.tar.gz | tar xvz -C ~/.julia
```

## Install

```bash
./install --shell zsh --gnome kali --neovim v0.10.3
*** Install dotfiles for user : yoda
- Checking dependencies ok
- Install fonts v3.2.1
- Install tmux
- Install screen
- Install gitconfig
- Install golang go1.23.6
- Install cheat
- Install shell zsh
- Install neovim v0.10.3 lazy
- Install gnome kali
*** Create symlink : ~/.local/bin/dotfiles
```
