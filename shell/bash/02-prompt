# vim: filetype=sh

#===============================#
#   PROMPT                      #
#===============================#

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color|linux) color_prompt=yes;;
esac

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    color_prompt=yes
else
    color_prompt=
fi

# reset
rst="$(tput sgr0)"

# colors
red="$(tput setaf 1)"
gre="$(tput setaf 2)"
cya="$(tput setaf 6)"
whi="$(tput setaf 7)"

PS1='\u@\h:\w\$ '
PROMPT_ALTERNATIVE=backtrack
if [ "$color_prompt" = yes ]; then
    [ "$EUID" -eq 0 ] && symbol=💀 || symbol=㉿
    [ "$EUID" -eq 0 ] && color=$red || color=$gre
    VIRTUAL_ENV_DISABLE_PROMPT=1
    PROMPT_COMMAND=set_prompt
fi

set_prompt()
{
    case "$PROMPT_ALTERNATIVE" in
        legacy)     PS1=$(printf "%s $ "    "$(set_legacy)" );;
        backtrack)  PS1=$(printf "\n%s\n❯ " "$(set_backtrack)" );;
        kali)       PS1=$(printf "\n%s❯ "   "$(set_kali)" );;
    esac
}

set_legacy()
{
    echo "$(tput bold)${color}\h${cya}\w${rst}"
}

set_kali()
{
    echo "$(tput bold)${color}┌── ${whi}\t ${color}\u${symbol}\h ${cya}\w\n${color}└─${rst}"
}

set_backtrack()
{
    echo "\t $(tput bold)${color}\u${symbol}\h ${cya}\w${rst}"
}

unset color_prompt

if [ -x ~/.prompt/bash/gitprompt.sh ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    #GIT_PROMPT_FETCH_REMOTE_STATUS=0
    GIT_PROMPT_SHOW_UPSTREAM=1
    GIT_PROMPT_SHOW_UNTRACKED_FILES=all     # no, normal or all
    GIT_PROMPT_SHOW_CHANGED_FILES_COUNT=0
    GIT_PROMPT_THEME=Single_line
    #GIT_PROMPT_THEME_FILE=~/.prompt/prompt-colors.sh
    source ~/.prompt/bash/gitprompt.sh
fi
